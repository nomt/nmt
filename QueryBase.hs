module QueryBase where

import World
import Table

type QueryPruner d = WorldNode -> d -> Bool
type QueryFunc u d = WorldNode -> d -> [Maybe u] -> [Maybe u] -> u
type QueryDistributor d c = WorldNode -> d -> c -> [d]
type QueryArrayDistributor d = QueryDistributor d [WorldNode]
type QueryTableDistributor d = QueryDistributor d (Table WorldNode)
type Query u d = (QueryPruner d, QueryFunc u d, QueryArrayDistributor d, QueryTableDistributor d, d)

worldQuery :: WorldNode -> Query a b -> a
worldQuery (Saved s) (_, query_func, _, _, b) = query_func (Saved s) b [] []
worldQuery Hidden (_, query_func, _, _, b) = query_func Hidden b [] []
worldQuery (Present (Node v arr tab)) (skip_func, query_func, arr_func, tab_func, b) = query_func my_wn b (zipWith z1 mq_arr b_arr)  (zipWith z2 mq_tab b_tab)
    where my_wn :: WorldNode
          my_wn = (Present (Node v arr tab))
          mq = \wn c -> ((skipElement wn (skip_func wn c)) >>= (\wn2 -> Just (worldQuery wn2 (skip_func, query_func, arr_func, tab_func, c))))
          z1 = \f c -> f c
          z2 = \(s,f) c -> f c
          mq_arr = map mq arr
          mq_tab = map (\(s,wn) -> (s, mq wn)) tab
          b_arr = arr_func my_wn b arr
          b_tab = tab_func my_wn b tab
          skipElement a s | s = Nothing
                          | otherwise = Just a

