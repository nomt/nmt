module QueryFunctions where

import Helpers as Hlp
import Table
import World
import QueryBase

maybeUpdateList :: [a] -> [Maybe a] -> [a]
maybeUpdateList xs ys = zipWith Hlp.maybeUpdate xs ys

maybeUpdateTable :: Table a -> [Maybe a] -> Table a
maybeUpdateTable xs ys = zipWith (\(k, v) y -> (k, Hlp.maybeUpdate v y)) xs ys

assembleNodef :: (NodeBulk -> arg -> a) -> (String -> arg -> a) -> (arg -> a) -> WorldNode -> arg -> a
assembleNodef bulkf savedf hiddenf (Present nb) arg = bulkf nb arg
assembleNodef bulkf savedf hiddenf (Saved s) arg = savedf s arg
assembleNodef bulkf savedf hiddenf Hidden arg = hiddenf arg

type Modifier a args = a -> args -> a

maybeUpdateNodeBulk :: Modifier NodeBulk ([Maybe WorldNode], [Maybe WorldNode])
maybeUpdateNodeBulk (Node v arr tab) (arr', tab') = Node v (maybeUpdateList arr arr') (maybeUpdateTable tab tab')

assembleNodeModifier :: Modifier NodeBulk args -> Modifier String args -> Modifier WorldNode args
assembleNodeModifier nbm sm = assembleNodef (\nb a -> Present $ nbm nb a) (\s a -> Saved $ sm s a) (\_ -> Hidden)

presentModifier :: Modifier NodeBulk args -> Modifier WorldNode args
presentModifier nbm = assembleNodeModifier nbm (\s _ -> s)

maybeUpdateNode :: Modifier WorldNode ([Maybe WorldNode], [Maybe WorldNode])
maybeUpdateNode = presentModifier maybeUpdateNodeBulk

modifierQuery :: Modifier WorldNode ([Maybe WorldNode], [Maybe WorldNode]) -> QueryFunc WorldNode b -> QueryFunc WorldNode b
modifierQuery m q = \wn b arr tab -> q (m wn (arr, tab)) b arr tab

finderQuery :: QueryFunc (Maybe a) b
finderQuery wn d arr tab = getOne dat
    where dat = Hlp.delUseless (map removeDoubleMaybe (arr ++ tab)) Hlp.isJust
          getOne [] = Nothing
          getOne [Just x] = Just x
          getOne _ = Nothing

type NodeChooser u d = WorldNode -> d -> [Maybe u] -> [Maybe u] -> Bool

mixQueryFunctions :: NodeChooser u d -> QueryFunc u d -> QueryFunc u d -> QueryFunc u d
mixQueryFunctions nc qf1 qf2 = qf
    where qf wn b arr tab | nc wn b arr tab = qf1 wn b arr tab
                          | otherwise = qf2 wn b arr tab

queryEndsThereChooser :: NodeChooser u d
queryEndsThereChooser _ _ xs ys = not $ (Hlp.listHasJust xs) || (Hlp.listHasJust ys)

unloadedNodeChooser :: NodeChooser u d
unloadedNodeChooser (Present _) _ _ _ = False
unloadedNodeChooser _ _ _ _ = True

downTypeChooser :: (d -> Bool) -> NodeChooser u d
downTypeChooser f _ d _ _ = f d

