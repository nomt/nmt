module Adress where

import Data.Word

data AdressPiece = AP8 [Word8] | APInt Int | APString String deriving Show
type Adress = [AdressPiece]

instance Eq AdressPiece where
    AP8 x == AP8 y = x == y
    APInt x == APInt y = x == y
    APString x == APString y = x == y
    AP8 [x] == APInt y = fromIntegral x == fromIntegral y
    APInt x == AP8 [y] = fromIntegral x == fromIntegral y
    _ == _ = False

adressPush :: Adress -> AdressPiece -> Adress
adressPush a (AP8 w8s) = reverse $ adressPushR (reverse a) w8s
    where adressPushR :: Adress -> [Word8] -> Adress
          adressPushR ((AP8 w8s'):as) w8s'' = (AP8 (w8s' ++ w8s'')):as
          adressPushR a' w8s'' = (AP8 w8s''):a'
adressPush a a2 = a ++ [a2]

adressHead :: Adress -> AdressPiece
adressHead ((AP8 (w8:w8s)):as) = AP8 [w8]
adressHead (a:as) = a

adressTail :: Adress -> Adress
adressTail ((AP8 [w8]):as) = as
adressTail ((AP8 (w8:w8s)):as) = (AP8 w8s):as
adressTail (a:as) = as

adressCompare :: Adress -> Adress -> (Adress, Adress, Adress)
adressCompare a b = ac a b []
    where ac a [] c = (c, a, [])
          ac [] b c = (c, [], b)
          ac a b c | ah == bh = ac (adressTail a) (adressTail b) (adressPush c ch)
                   | otherwise = (c, a, b)
                   where ah = adressHead a
                         bh = adressHead b
                         ch = smaller ah bh
                         smaller (AP8 x) (APInt y) = AP8 x
                         smaller (APInt x) (AP8 y) = AP8 y
                         smaller x _ = x

