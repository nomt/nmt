module World where

import Data.Word
import Helpers as Hlp
import Table
import Adress

data Value = VInt Int | VFloat Float | VString String deriving (Show, Eq)

data WorldNode = Present NodeBulk | Saved String | Hidden deriving (Show, Eq)
data NodeBulk = Node Value [WorldNode] (Table(WorldNode)) deriving (Show, Eq)

getSubnode :: WorldNode -> Adress -> Maybe WorldNode
getSubnode wn [] = Just wn
getSubnode (Saved _) _ = Nothing
getSubnode Hidden _ = Nothing
getSubnode (Present (Node v arr tab)) (AP8 (ap8:ap8s):aps) = (arr ^? ap8) >>= (\(vv) -> getSubnode vv (AP8 ap8s:aps))
getSubnode (Present (Node v arr tab)) (APInt i:aps) = (arr ^? i) >>= (\(vv) -> getSubnode vv aps)
getSubnode (Present (Node v arr tab)) (APString s:aps) = (findKey tab s) >>= (\(vv) -> getSubnode vv aps)

(^?) :: Integral i => [v] -> i -> Maybe v
[] ^? _ = Nothing
(v:vs) ^? i | i == 0 = Just v
            | i > 0 = vs ^? (i-1)
            | otherwise = Nothing

