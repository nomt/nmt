module QueryDistributors where

import Data.Word
import QueryBase
import Adress

word8Distributor :: QueryArrayDistributor Word8
word8Distributor _ _ arr = [0..((fromIntegral $ length arr) - 1)]

intDistributor :: QueryArrayDistributor Int
intDistributor _ _ arr = [0..((length arr) - 1)]

keyDistributor :: QueryTableDistributor String
keyDistributor _ _ kvs = map (\(k, v) -> k) kvs

adressPieceArrayDistributor :: QueryArrayDistributor AdressPiece
adressPieceArrayDistributor wn _ arr | length arr < 256 = map (\w8 -> AP8 [w8]) (word8Distributor wn 0 arr)
                                     | otherwise = map (\i -> APInt i) (intDistributor wn 0 arr)

adressPieceTableDistributor :: QueryTableDistributor AdressPiece
adressPieceTableDistributor wn _ tab = map (\s -> APString s) (keyDistributor wn "" tab)

adressArrayDistributor :: QueryArrayDistributor Adress
adressArrayDistributor wn b arr = map (\ap -> adressPush b ap) (adressPieceArrayDistributor wn (AP8 []) arr)

adressTableDistributor :: QueryTableDistributor Adress
adressTableDistributor wn b arr = map (\ap -> adressPush b ap) (adressPieceTableDistributor wn (AP8 []) arr)

adressArrayDistRemover :: QueryArrayDistributor (Maybe Adress)
adressArrayDistRemover wn Nothing arr = map (\ap -> Nothing) arr
adressArrayDistRemover wn (Just a) arr = map (dec a) (adressPieceArrayDistributor wn (AP8 []) arr)
    where dec [] ap = Nothing
          dec a ap | (adressHead a) == ap = Just $ adressTail a
                   | otherwise = Nothing

adressTableDistRemover :: QueryTableDistributor (Maybe Adress)
adressTableDistRemover wn Nothing arr = map (\ap -> Nothing) arr
adressTableDistRemover wn (Just a) arr = map (dec a) (adressPieceTableDistributor wn (AP8 []) arr)
    where dec [] ap = Nothing
          dec a ap | (adressHead a) == ap = Just $ adressTail a
                   | otherwise = Nothing

noArrayDistributor :: QueryArrayDistributor ()
noArrayDistributor _ _ arr = map (\_ -> ()) arr

noTableDistributor :: QueryTableDistributor ()
noTableDistributor _ _ tab = map (\_ -> ()) tab

