module Table where

import Helpers as Hlp

type Table v = [(String, v)]

findKey :: Table(v) -> String -> Maybe v
findKey [] _ = Nothing
findKey ((k, v):kvs) key | k == key = Just v
                         | otherwise = findKey kvs key

hasKey :: Table(v) -> String -> Bool
hasKey tab k = Hlp.isJust $ findKey tab k

setKey :: Table(v) -> String -> v -> Table(v)
setKey tab k v | hasKey tab k = map (\(k', v') -> Hlp.boolChoose (k' == k) (k', v) (k', v')) tab
               | otherwise = (k, v):tab

