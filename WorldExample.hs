module WorldExample where

import World
import QueryBase
import Query
import Adress
import QueryFunctions

w0 = Present(Node (VInt 0) [] [])
w1 = Present(Node (VInt 1) [] [])
w2 = Saved "not really"
w3 = Hidden
w4 = Present(Node (VString "o_O") [w0, w1, w0, w3] [("E1", w2), ("E2", w0)])
w5 = Present(Node (VString "O_O") [w4] [])

wmmod :: WorldNode -> WorldNode
wmmod wn = presentModifier o_O wn ()
    where o_O :: Modifier NodeBulk ()
          o_O (Node (VInt i) a t) _ = Node (VInt (i+6)) a t
          o_O (Node a b c) _ = Node a b c

q1 = worldUpdateQuery wmmod (\wn _ _ _ -> wn)

addr1 = [AP8 [0,0]]

q2 = getNodeQuery addr1

