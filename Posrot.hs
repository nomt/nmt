module Posrot where

import Linear.V3
import Linear.Quaternion
import Linear.Metric

data Posrot = PR (V3 Double) (Quaternion Double)

posrotStep :: Posrot -> V3 Double -> Posrot
posrotStep (PR pos (Quaternion r (V3 i j k))) step = PR (pos + pos') rot
    where rot' = Quaternion r (V3 (-i) (-j) (-k))
          rot = Quaternion r (V3 i j k)
          (Quaternion _ pos') = rot*(Quaternion 0 step)*rot'

posrotRotate :: Posrot -> Quaternion Double -> Posrot
posrotRotate (PR pos rot) rot' = PR pos (fmap (/(norm r)) r)
    where r = rot*rot'

