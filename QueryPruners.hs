module QueryPruners where

import World
import QueryBase

queryAll :: QueryPruner b
queryAll _ _ = False

queryPresentOnly :: QueryPruner b
queryPresentOnly (Saved _) _ = True
queryPresentOnly Hidden _ = True
queryPresentOnly _ _ = False

pruneNothing :: QueryPruner (Maybe b)
pruneNothing _ Nothing = True
pruneNothing _ _ = False

constQuery :: a -> QueryFunc a b
constQuery a _ _ _ _ = a

