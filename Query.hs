module Query where

import Adress
import World
import QueryBase
import QueryPruners
import QueryFunctions
import QueryDistributors
import QueryAddSlot

adressEmptyChooser :: NodeChooser u (Maybe Adress)
adressEmptyChooser = downTypeChooser ae
    where ae (Just []) = True
          ae _ = False

straightQuery :: Adress -> Query () (Maybe Adress)
straightQuery a = (pruneNothing, \wn d l1 l2 -> (), adressArrayDistRemover, adressTableDistRemover, Just a)

getNodeQuery :: Adress -> Query (Maybe WorldNode) (Maybe Adress)
getNodeQuery a = (pruneNothing, qf, adressArrayDistRemover, adressTableDistRemover, Just a)
    where qf = mixQueryFunctions adressEmptyChooser (\wn _ _ _ -> Just wn) finderQuery

worldUpdateQuery :: (WorldNode -> WorldNode) -> QueryFunc WorldNode () -> Query WorldNode ()
worldUpdateQuery f afterUpdate = (queryAll, qf, noArrayDistributor, noTableDistributor, ())
    where qf = mixQueryFunctions queryEndsThereChooser (\wn b a t -> afterUpdate (f wn) b a t) (modifierQuery maybeUpdateNode afterUpdate)

