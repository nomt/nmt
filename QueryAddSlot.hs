module QueryAddSlot where

import QueryBase

queryAddUpSlot :: Query u1 d -> (QueryFunc u1 d -> QueryFunc (u1, u2) d) -> Query (u1, u2) d
queryAddUpSlot (qp, qf, qad, qtd, d) qff = (qp, qff qf, qad, qtd, d)

queryAddDownSlot :: Query u d1 -> (QueryPruner d1 -> QueryPruner (d1, d2)) -> (QueryFunc u d1 -> QueryFunc u (d1, d2)) -> (QueryArrayDistributor d1 -> QueryArrayDistributor (d1, d2)) -> (QueryTableDistributor d1 -> QueryTableDistributor (d1, d2)) -> d2 -> Query u (d1, d2)
queryAddDownSlot (qp, qf, qad, qtd, d) qpf qff qadf qtdf d' = (qpf qp, qff qf, qadf qad, qtdf qtd, (d, d'))

queryDistributorJoin :: QueryDistributor d2 c -> QueryDistributor d1 c -> QueryDistributor (d1, d2) c
queryDistributorJoin qd2 qd1 = \wn (d1, d2) awn -> zipWith (\x y -> (x,y)) (qd1 wn d1 awn) (qd2 wn d2 awn)

queryFuncJoinUp :: QueryFunc u2 d -> QueryFunc u1 d -> QueryFunc (u1, u2) d
queryFuncJoinUp qf2 qf1 = \wn d l1 l2 -> (qf1 wn d (map p1 l1) (map p1 l2), qf2 wn d (map p2 l1) (map p2 l2))
    where p1 Nothing = Nothing
          p1 (Just (x, _)) = Just x
          p2 Nothing = Nothing
          p2 (Just (_, y)) = Just y

queryFuncJoinDown :: (u -> u -> u) -> QueryFunc u d2 -> QueryFunc u d1 -> QueryFunc u (d1, d2)
queryFuncJoinDown f qf2 qf1 = \wn (d1, d2) l1 l2 -> f (qf1 wn d1 l1 l2) (qf2 wn d2 l1 l2)

queryPrunerJoin :: (Bool -> Bool -> Bool) -> QueryPruner d2 -> QueryPruner d1 -> QueryPruner (d1, d2)
queryPrunerJoin f qp2 qp1 = \wn (d1, d2) -> f (qp1 wn d1) (qp2 wn d2)

queryFuncExtendDown :: QueryFunc u d1 -> QueryFunc u (d1, d2)
queryFuncExtendDown qf1 = \wn (d1, d2) l1 l2 -> qf1 wn d1 l1 l2

