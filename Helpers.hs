module Helpers where

import Data.Word

boolChoose :: Bool -> a -> a -> a
boolChoose True x y = x
boolChoose False x y = y

delUseless :: [a] -> (a -> Bool) -> [a]
delUseless arr useful = reverse (delUselessR arr useful [])
    where delUselessR :: [a] -> (a -> Bool) -> [a] -> [a]
          delUselessR [] _ res = res
          delUselessR (a:as) f res | f a = delUselessR as f (a:res)
                                   | otherwise = delUselessR as f res

isJust :: Maybe a -> Bool
isJust Nothing = False
isJust _ = True

isJustStr :: (String, Maybe a) -> Bool
isJustStr (_, Nothing) = False
isJustStr _ = True

maybeUpdate :: a -> Maybe a -> a
maybeUpdate x Nothing = x
maybeUpdate _ (Just y) = y

listHasAtLeast :: [a] -> Int -> (a -> Bool) -> Bool
listHasAtLeast _ 0 _ = True
listHasAtLeast [] _ _ = False
listHasAtLeast (a:as) i cond | cond a = listHasAtLeast as (i-1) cond
                             | otherwise = listHasAtLeast as i cond

listHasAtMost :: [a] -> Int -> (a -> Bool) -> Bool
listHasAtMost as i cond = not $ listHasAtLeast as (i+1) cond

listHasJust :: [Maybe a] -> Bool
listHasJust as = listHasAtLeast as 1 isJust

removeDoubleMaybe :: Maybe (Maybe a) -> Maybe a
removeDoubleMaybe Nothing = Nothing
removeDoubleMaybe (Just x) = x

